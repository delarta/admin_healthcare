import React from 'react';
import {BrowserRouter as Router, Route} from 'react-router-dom'
import SignIn from './pages/SignInPage';
import Dashboard from "./pages/Dashboard";
import Personnel from "./pages/PersonnelManager";
import Provider from "./pages/ProviderManager";
import Facilities from "./pages/FacilitiesManager";
function App() {
  return (
    <div>
      <Router>
      <Route exact path="/" component={Dashboard} />
        <Route path="/personnel" component={Personnel} />
        <Route path="/provider" component={Provider} />
        <Route path="/facilities" component={Facilities} />
        <Route path="/signin" component={SignIn}/>

      </Router>
    </div>
  );
}

export default App;
