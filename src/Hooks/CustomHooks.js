import { useState } from "react";

const useSignInForm = callback => {
  const [inputs, setInputs] = useState({ username: "", password: "" });
  const handleSubmit = event => {
    if (event) {
      event.preventDefault();
    }
    callback();
  };

  const handleInputChange = event => {
    event.persist();
    setInputs(inputs => ({
      ...inputs,
      [event.target.name]: [event.target.value]
    }));
  };
  return {
    handleSubmit,
    handleInputChange,
    inputs
  };
};

export const useCreatePersonnel = callback => {
  const [inputs, setInputs] = useState({
    firstName: "",
    lastName: "",
    NRIC: "",
    designation: "",
    mobileNumber: "",
    type:"",
    mcrNumber: "",
    descriptionAndRemarks: "",
    yearsOfExperience: "",
    certificationName: "",
    remarks: "",
    defaultSpecialty: "",
    subSpecialties: ""
  });
  const handleSubmit = event => {
    if (event) {
      event.preventDefault();
    }
    callback();
  };

  const handleInputChange = event => {
    event.persist();
    setInputs(inputs => ({
      ...inputs,
      [event.target.name]: [event.target.value]
    }));
  };
  return {
    handleSubmit,
    handleInputChange,
    setInputs,
    inputs
  };
};

export const useCreateFacility = callback => {
  const [inputs, setInputs] = useState({
    facilityName: ""
  });
  const handleSubmit = event => {
    if (event) {
      event.preventDefault();
    }
    callback();
  };

  const handleInputChange = event => {
    event.persist();
    setInputs(inputs => ({
      ...inputs,
      [event.target.name]: [event.target.value]
    }));
  };
  return {
    handleSubmit,
    handleInputChange,
    inputs
  };
};

export const useCreateProvider = callback => {
  const [inputs, setInputs] = useState({
    clinicName: "",
    type: "",
    telephone: "",
    email: "",
    gstStatus: ""
  });
  const handleSubmit = event => {
    if (event) {
      event.preventDefault();
    }
    callback();
  };

  const handleInputChange = event => {
    event.persist();
    setInputs(inputs => ({
      ...inputs,
      [event.target.name]: [event.target.value]
    }));
  };
  return {
    handleSubmit,
    handleInputChange,
    inputs
  };
};
export default useSignInForm;
