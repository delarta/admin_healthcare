import React from "react";
import ReactDOM from "react-dom";

import "./assets/css/_main.scss";

import App from "./App";
import rootReducer from "./redux/reducers/index";
import { Provider } from "react-redux";
import { createStore, applyMiddleware, compose } from "redux";
import thunk from 'redux-thunk';

import * as serviceWorker from "./serviceWorker";

const store = {
  ...createStore(
    rootReducer,
    compose(
      applyMiddleware(thunk),
      ...(window.__REDUX_DEVTOOLS_EXTENSION__
        ? [window.__REDUX_DEVTOOLS_EXTENSION__()]
        : [])
    )
  )
};

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById("root")
);

serviceWorker.unregister();
