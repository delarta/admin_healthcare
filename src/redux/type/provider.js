export const REQUEST_SHOW_PROVIDER_DATA = 'REQUEST_SHOW_PROVIDER_DATA';
export const REQUEST_CREATE_PROVIDER_DATA = 'REQUEST_CREATE_PROVIDER_DATA';
export const REQUEST_DELETE_PROVIDER_DATA = 'REQUEST_DELETE_PROVIDER_DATA';

export const SHOW_PROVIDER_DATA = 'SHOW_PROVIDER_DATA';
export const CREATE_PROVIDER_DATA = 'CREATE_PROVIDER_DATA';
export const DELETE_PROVIDER_DATA = 'DELETE_PROVIDER_DATA';
export const VIEW_PROVIDER_DATA = 'VIEW_PROVIDER_DATA';

export const FAILED_PROVIDER_DATA = 'FAILED_PROVIDER_DATA';

