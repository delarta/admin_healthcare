import {
  SHOW_PERSONNEL_DATA,
  VIEW_PERSONNEL_DATA,
  CREATE_PERSONNEL_DATA,
  DELETE_PERSONNEL_DATA
} from "../type/personnel";
import axios from "axios";
import config from "../../config";
const url = config.API_URL
const token = localStorage.getItem("Token");

export const showPersonnel = () => {
  return dispatch => {
    return axios({
      method: "get",
      url: `${url}/personnel`,
      headers: {
        "Content-Type": "application/json",
        "X-Content-Type-Options": "nosniff",
        Authorization: token
      },
      responseType: "json"
    }).then(res => {
      dispatch({ type: SHOW_PERSONNEL_DATA, payload: res.data.data });
      return res.data.data;
    });
  };
};

export const viewPersonnel = ID => {
  return dispatch => {
    return axios({
      method: "get",
      url: `${url}/personnel/${ID}`,
      headers: { "Content-Type": "application/json", Authorization: token },
      responseType: "json"
    }).then(res => {
      dispatch({ type: VIEW_PERSONNEL_DATA, payload: res.data.data[0] });
    });
  };
};

export const createPersonnel = data => {
  const {
    firstName,
    lastName,
    NRIC,
    designation,
    mobileNumber,
    MCRNumber,
    descriptionAndRemarks,
    yearsOfExperience,
    certificationName,
    type,
    remarks,
    defaultSpecialty,
    subSpecialties
  } = data;
  return dispatch => {
    return axios({
      method: "post",
      url: `${url}/personnel`,
      headers: { "Content-Type": "application/json", Authorization: token },
      data: {
        firstName,
        lastName,
        NRIC,
        designation,
        mobileNumber,
        MCRNumber,
        descriptionAndRemarks,
        yearsOfExperience,
        certificationName,
        type,
        remarks,
        defaultSpecialty,
        subSpecialties
      },
      responseType: "json"
    })
      .then(res => {
        dispatch({ type: CREATE_PERSONNEL_DATA, payload: res.data.data });
        alert("Personnel Successfully Created!");
      })
      .catch(err => {
        alert(`Failed to Create Personnel! ${err.response}`);
      });
  };
};

export const deletePersonnel = id => {
  return dispatch => {
    return axios({
      method: "delete",
      url: `${url}/personnel/${id}`,
      headers: { "Content-Type": "application/json", Authorization: token },
      responseType: "json"
    }).then(res => {
      dispatch({ type: DELETE_PERSONNEL_DATA, payload: res.data.doc.ID });
    });
  };
};
