import { SUCCESS_LOGIN } from "../type/auth";
import axios from "axios";
import config from "../../config";

const url = config.API_URL;
export const userLogin = data => {
  const { username, password } = data;
  return dispatch => {
    return axios({
      method: "post",
      url: `${url}/user/login`,
      headers: { "Content-Type": "application/json" },
      data: {
        username: username[0],
        password: password[0]
      },
      responseType: "json"
    })
      .then(res => {
        console.log(res.data);
        localStorage.setItem("Token", res.data.token);
        dispatch({ type: SUCCESS_LOGIN, payload: res.data.token });
        setTimeout(()=>{},1000)
      })
      .catch(err => {
        alert(`Login Failed! ${err.response}`);
      });
  };
};
