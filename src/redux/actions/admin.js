
  import axios from "axios";
import config from "../../config";
  const url = config.API_URL;
  const token = localStorage.getItem("Token");
  
  export const getPersonnel = () => {
    return dispatch => {
      return axios({
        method: "get",
        url: `${url}/personnel`,
        headers: {
          "Content-Type": "application/json",
          "X-Content-Type-Options": "nosniff",
          Authorization: token
        },
        responseType: "json"
      }).then(res => {
          dispatch({ type: 'FETCH_PERSONNEL_DATA', payload: res.data.data });
      });
    };
  };

  export const getProvider = () => {
    return dispatch => {
      return axios({
        method: "get",
        url: `${url}/provider`,
        headers: {
          "Content-Type": "application/json",
          "X-Content-Type-Options": "nosniff",
          Authorization: token
        },
        responseType: "json"
      }).then(res => {
        dispatch({ type: 'FETCH_PROVIDER_DATA', payload: res.data.data });
      });
    };
  };

  export const getFacility = () => {
    return dispatch => {
      return axios({
          method: "get",
          url: `${url}/facility`,
          headers: {
            "Content-Type": "application/json",
            "X-Content-Type-Options": "nosniff",
            Authorization: token
          },
          responseType: "json"
        }).then(res => {
            dispatch({type: 'FETCH_FACILITY_DATA', payload: res.data.data})
        });
    };
  };