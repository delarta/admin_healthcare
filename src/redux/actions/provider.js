import {
  DELETE_PROVIDER_DATA,
  SHOW_PROVIDER_DATA,
  CREATE_PROVIDER_DATA,
  VIEW_PROVIDER_DATA
} from "../type/provider";
import axios from "axios";
import config from "../../config";

const url = config.API_URL
const token = localStorage.getItem("Token");

export const showProvider = () => {
  return dispatch => {
    return axios({
      method: "get",
      url: `${url}/provider`,
      headers: {
        "Content-Type": "application/json",
        "X-Content-Type-Options": "nosniff",
        Authorization: token
      },
      responseType: "json"
    }).then(res => {
      dispatch({ type: SHOW_PROVIDER_DATA, payload: res.data.data });
    });
  };
};

export const createProvider = data => {
  const { clinicName, type, contact, gstStatus } = data;
  return dispatch => {
    return axios({
      method: "post",
      url: `${url}/provider`,
      headers: { "Content-Type": "application/json", Authorization: token },
      data: {
        clinicName,
        type,
        contact: {
          telephone: contact.telephone,
          email: contact.email
        },
        gstStatus
      },
      responseType: "json"
    }).then(res => {
      alert("Data Successfully Added!");
      dispatch({ type: CREATE_PROVIDER_DATA, payload: res.data.data });
    });
  };
};

export const deleteProvider = id => {
  console.log(id);
  return dispatch => {
    return axios({
      method: "delete",
      url: `${url}/provider/${id}`,
      headers: { "Content-Type": "application/json", Authorization: token },
      responseType: "json"
    }).then(res => {
      dispatch({ type: DELETE_PROVIDER_DATA, payload: res.data.doc.ID });
    });
  };
};

export const viewProvider = ID => {
  console.log(ID);
  return dispatch => {
    return axios({
      method: "get",
      url: `${url}/provider/${ID}`,
      headers: { "Content-Type": "application/json", Authorization: token },
      responseType: "json"
    }).then(res => {
      dispatch({ type: VIEW_PROVIDER_DATA, payload: res.data.data[0] });
    });
  };
};
