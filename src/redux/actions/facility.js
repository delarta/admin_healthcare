import {
  SHOW_FACILITY_DATA,
  CREATE_FACILITY_DATA,
  DELETE_FACILITY_DATA,
  UPDATE_FACILITY_DATA
} from "../type/facility";
import axios from "axios";
import config from "../../config";

const url = config.API_URL;
const token = localStorage.getItem("Token");

export const showFacility = () => {
  return dispatch => {
    return axios({
      method: "get",
      url: `${url}/facility`,
      headers: {
        "Content-Type": "application/json",
        "X-Content-Type-Options": "nosniff",
        Authorization: token
      },
      responseType: "json"
    }).then(res => {
      dispatch({ type: SHOW_FACILITY_DATA, payload: res.data.data });
    });
  };
};

export const createFacility = data => {
  return dispatch => {
    return axios({
      method: "post",
      url: `${url}/facility`,
      headers: { "Content-Type": "application/json", Authorization: token },
      data: {
        facilityName: data.facilityName
      },
      responseType: "json"
    })
      .then(res => {
        alert(`Successfully Created Facility!`);
        dispatch({ type: CREATE_FACILITY_DATA, payload: res.data.datas });
      })
      .catch(err => {
        alert(`Failed to Create Facility! ${err.response}`);
      });
  };
};

export const updateFacility = data => {
  const { ID, facilityName } = data;
  return dispatch => {
    return axios({
      method: "put",
      url: `${url}/facility/${ID}`,
      headers: { "Content-Type": "application/json", Authorization: token },
      data: {
        facilityName
      },
      responseType: "json"
    })
      .then(res => {
        alert(`Successfully Updated Facility!`);
        dispatch({ type: UPDATE_FACILITY_DATA, payload: res.data.datas });
      })
      .catch(err => {
        alert(`Failed to Update Facility! ${err.response}`);
      });
  };
};

export const deleteFacility = id => {
  return dispatch => {
    return axios({
      method: "delete",
      url: `${url}/facility/${id}`,
      headers: { "Content-Type": "application/json", Authorization: token },
      responseType: "json"
    })
      .then(res => {
        alert(`Successfully Deleted Facility with ID ${id}!`);
        dispatch({ type: DELETE_FACILITY_DATA, payload: res.data.doc.ID });
      })
      .catch(err => {
      });
  };
};
