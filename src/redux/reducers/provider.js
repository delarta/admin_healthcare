import {
  SHOW_PROVIDER_DATA,
  VIEW_PROVIDER_DATA,
  CREATE_PROVIDER_DATA,
  DELETE_PROVIDER_DATA,
  FAILED_PROVIDER_DATA
} from "../type/provider";
const initialState = {
  data: [],
  viewData: {},
  loading: false
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SHOW_PROVIDER_DATA:
      return {
        ...state,
        data: action.payload,
        loading: false
      };
    case VIEW_PROVIDER_DATA:
      return {
        ...state,
        viewData: action.payload
      };
    case CREATE_PROVIDER_DATA:
      return {
        ...state,
        data: [...state.data, action.payload],
        loading: false
      };
    case DELETE_PROVIDER_DATA:
      return {
        ...state,
        data: [...state.data.filter(item => item.ID !== action.payload)],
        loading: false
      };
    case FAILED_PROVIDER_DATA:
      return {
        ...state,
        data: action.payload,
        loading: false
      };
    default:
      return {
        state
      };
  }
};
