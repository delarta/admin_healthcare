const initialState = {
  personnels: [],
  facilities: [],
  providers: []
};

const adminReducer = (state = initialState, action) => {
  switch (action.type) {
    case "FETCH_PERSONNEL_DATA":
      return {
        ...state,
        personnels: action.payload
      };
    case "FETCH_FACILITY_DATA":
      return {
        ...state,
        facilities: action.payload
      };
    case "FETCH_PROVIDER_DATA":
      return {
        ...state,
        providers: action.payload
      };
    default: return state
  }
};

export default adminReducer
