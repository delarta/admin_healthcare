import {
  REQUEST_LOGIN,
  SUCCESS_LOGIN,
  FAILED_LOGIN,
  LOG_OUT
} from "../type/auth";
const initialState = {
  isAdmin: false,
  token: ''
};

const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case REQUEST_LOGIN:
      return {
        ...state,
        loading: true
      };
    case SUCCESS_LOGIN:
      localStorage.setItem('Token', action.payload);
      return {
        ...state,
        isAdmin: true,
        token: localStorage.getItem('Token')
      };
    case FAILED_LOGIN:
      return {
        ...state
      };
    case LOG_OUT:
      localStorage.removeItem('Token');
      localStorage.removeItem("firstLoad");
      return {
        ...state,
        isAdmin: false,
        token: ''
      };
    default:
      return state;
  }
};

export default authReducer;
