import {
  VIEW_PERSONNEL_DATA,
  SHOW_PERSONNEL_DATA,
  CREATE_PERSONNEL_DATA,
  DELETE_PERSONNEL_DATA,
  FAILED_PERSONNEL_DATA
} from "../type/personnel";
const initialState = {
  data: [],
  viewData: {},
  loading: false
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SHOW_PERSONNEL_DATA:
      return {
        ...state,
        data: action.payload,
        loading: false
      };

    case VIEW_PERSONNEL_DATA:
      return {
        ...state,
        viewData: action.payload
      };

    case CREATE_PERSONNEL_DATA:
      return {
        ...state,
        data: [...state.data, action.payload],
        loading: false
      };
    case DELETE_PERSONNEL_DATA:
      return {
        ...state,
        data: [...state.data.filter(item => item.ID !== action.payload)],
        loading: false
      };
    case FAILED_PERSONNEL_DATA:
      return {
        ...state,
        data: action.payload,
        loading: false
      };
    default:
      return {
        state
      };
  }
};
