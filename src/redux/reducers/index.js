import {combineReducers} from 'redux'
import auth from './auth'
import personnel from './personnel'
import facility from './facility'
import provider from './provider'
import admin from './admin'

const indexReducer = combineReducers({
    auth: auth,
    personnel: personnel,
    facility: facility,
    provider: provider,
    admin: admin
})

export default indexReducer