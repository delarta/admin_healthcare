import {
  REQUEST_CREATE_FACILITY_DATA,
  REQUEST_DELETE_FACILITY_DATA,
  REQUEST_SHOW_FACILITY_DATA,
  REQUEST_UPDATE_FACILITY_DATA,
  SHOW_FACILITY_DATA,
  CREATE_FACILITY_DATA,
  DELETE_FACILITY_DATA,
  FAILED_FACILITY_DATA,
  UPDATE_FACILITY_DATA
} from "../type/facility";

const initialState = {
  data: [],
  loading: false
};

export default (state = initialState, action) => {
  switch (action.type) {
    case REQUEST_SHOW_FACILITY_DATA:
      return {
        ...state,
        loading: true
      };
    case REQUEST_CREATE_FACILITY_DATA:
      return {
        ...state,
        loading: true
      };
    case REQUEST_DELETE_FACILITY_DATA:
      return {
        ...state,
        loading: true
      };

    case REQUEST_UPDATE_FACILITY_DATA:
      return {
        ...state,
        loading: true
      };

    case SHOW_FACILITY_DATA:
      return {
        ...state,
        data: action.payload,
        loading: false
      };
    case CREATE_FACILITY_DATA:
      return {
        ...state,
        data: [...state.data, action.payload],
        loading: false
      };
    case DELETE_FACILITY_DATA:
      return {
        ...state,
        data: [...state.data.filter(item => item.ID !== action.payload)],
        loading: false
      };
    case UPDATE_FACILITY_DATA:
      return {
        ...state,
        data: [
          ...state.data.map(item => {
            if (item.ID === action.payload.ID) {
              item = action.payload;
            }
            return item;
          })
        ],
        loading: false
      };
    case FAILED_FACILITY_DATA:
      return {
        ...state,
        data: action.payload,
        loading: false
      };
    default:
      return {
        state
      };
  }
};
