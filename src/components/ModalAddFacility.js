import React from "react";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import { useTheme } from "@material-ui/core/styles";
import { TextField } from "@material-ui/core";
import { useCreateFacility } from "../Hooks/CustomHooks";
import {createFacility} from "../redux/actions/facility"
import {connect} from 'react-redux'

function ResponsiveDialog(props) {
  const [open, setOpen] = React.useState(false);

  const create = () => {
    const datas = {
      facilityName: inputs.facilityName[0]
    };
    props.createFacility(datas);
  };

  const { inputs, handleInputChange, handleSubmit } = useCreateFacility(
    create
  );
  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down("sm"));

  function handleClickOpen() {
    setOpen(true);
  }

  function handleClose() {
    setOpen(false);
  }

  return (
    <div>
      <Button color="primary" variant="contained" onClick={handleClickOpen}>
        Create Facility
      </Button>
      <Dialog
        fullScreen={fullScreen}
        open={open}
        onClose={handleClose}
        aria-labelledby="responsive-dialog-title"
      >
        <DialogTitle id="responsive-dialog-title">
          {"Create Facility"}
        </DialogTitle>
        <DialogContent>
          <form onSubmit={handleSubmit}>
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="facilityName"
              label="Facility Name"
              name="facilityName"
              autoComplete="facilityName"
              autoFocus
              onChange={handleInputChange}
              value={inputs.facilityName}
            />
           
            <Button type="submit" fullWidth variant="contained" color="primary">
              Create
            </Button>
          </form>
        </DialogContent>
      </Dialog>
    </div>
  );
}

const mapDispatchToProps = dispatch =>{
    return{
        createFacility: (datas) => dispatch(createFacility(datas))
    }
}

export default connect(null, mapDispatchToProps)(ResponsiveDialog)