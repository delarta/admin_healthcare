import React from "react";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import { useTheme } from "@material-ui/core/styles";
import { TextField } from "@material-ui/core";
import { useCreatePersonnel } from "../Hooks/CustomHooks";
import { createPersonnel } from "../redux/actions/personnel";
import { connect } from "react-redux";

function ResponsiveDialog(props) {
  const [open, setOpen] = React.useState(false);

  const create = () => {
    const datas = {
      firstName: inputs.firstName[0],
      lastName: inputs.lastName[0],
      NRIC: inputs.NRIC[0],
      designation: inputs.designation[0],
      mobileNumber: inputs.mobileNumber[0],
      MCRNumber: inputs.mcrNumber[0],
      descriptionAndRemarks: inputs.descriptionAndRemarks[0],
      yearsOfExperience: inputs.yearsOfExperience[0],
      certificationName: inputs.certificationName[0],
      type: inputs.type[0],
      remarks: inputs.remarks[0],
      defaultSpecialty: inputs.defaultSpecialty[0],
      subSpecialties: inputs.subSpecialties[0]
    };
    props.createPersonnel(datas);
    setInputs(() => ({
      firstName: "",
      lastName: "",
      NRIC: "",
      designation: "",
      mobileNumber: "",
      mcrNumber: "",
      type:"",
      descriptionAndRemarks: "",
      yearsOfExperience: "",
      certificationName: "",
      remarks: "",
      defaultSpecialty: "",
      subSpecialties: ""
    }));
    handleClose()
  };

  const {
    inputs,
    handleInputChange,
    handleSubmit,
    setInputs
  } = useCreatePersonnel(create);
  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down("sm"));

  function handleClickOpen() {
    setOpen(true);
  }

  function handleClose() {
    setOpen(false);
  }

  return (
    <div>
      <Button color="primary" variant="contained" onClick={handleClickOpen}>
        Create Personnel
      </Button>
      <Dialog
        fullScreen={fullScreen}
        open={open}
        onClose={handleClose}
        aria-labelledby="responsive-dialog-title"
      >
        <DialogTitle id="responsive-dialog-title">
          {"Create Personnel"}
        </DialogTitle>
        <DialogContent>
          <form onSubmit={handleSubmit}>
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="firstName"
              label="First Name"
              name="firstName"
              autoComplete="firstName"
              autoFocus
              onChange={handleInputChange}
              value={inputs.firstName}
            />
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="lastnNme"
              label="Last Name"
              name="lastName"
              autoComplete="lastName"
              onChange={handleInputChange}
              value={inputs.lastName}
            />
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="NRIC"
              label="NRIC"
              name="NRIC"
              autoComplete="NRIC"
              onChange={handleInputChange}
              value={inputs.NRIC}
            />
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="type"
              label="Type"
              name="type"
              autoComplete="type"
              onChange={handleInputChange}
              value={inputs.type}
            />
            {inputs.type[0] !== undefined &&
              inputs.type[0].toLowerCase() === "doctor" && (
                <TextField
                  variant="outlined"
                  margin="normal"
                  required
                  fullWidth
                  id="mcrNumber"
                  label="MCR Number"
                  name="mcrNumber"
                  autoComplete="mcrNumber"
                  onChange={handleInputChange}
                  value={inputs.mcrNumber}
                />
              )}
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="designation"
              label="Designation"
              name="designation"
              autoComplete="designation"
              onChange={handleInputChange}
              value={inputs.designation}
            />
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="mobileNumber"
              label="Mobile Number"
              name="mobileNumber"
              autoComplete="mobileNumber"
              onChange={handleInputChange}
              value={inputs.mobileNumber}
            />
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="descriptionAndRemarks"
              label="Desc and Remark"
              name="descriptionAndRemarks"
              autoComplete="descriptionAndRemarks"
              onChange={handleInputChange}
              value={inputs.descriptionAndRemarks}
            />
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              type="number"
              id="yearsOfExperience"
              label="Years of Experience"
              name="yearsOfExperience"
              autoComplete="yearsOfExperience"
              onChange={handleInputChange}
              value={inputs.yearsOfExperience}
            />
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="certificationName"
              label="Certification"
              name="certificationName"
              autoComplete="certificationName"
              onChange={handleInputChange}
              value={inputs.certificationName}
            />
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="remarks"
              label="Remarks"
              name="remarks"
              autoComplete="remarks"
              onChange={handleInputChange}
              value={inputs.remarks}
            />
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="defaultSpecialty"
              label="Default Specialities"
              name="defaultSpecialty"
              autoComplete="defaultSpecialty"
              onChange={handleInputChange}
              value={inputs.defaultSpecialty}
            />
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="subSpecialties"
              label="Sub Specialties"
              name="subSpecialties"
              autoComplete="subSpecialties"
              onChange={handleInputChange}
              value={inputs.subSpecialties}
            />
            <Button type="submit" fullWidth variant="contained" color="primary">
              Create
            </Button>
          </form>
        </DialogContent>
      </Dialog>
    </div>
  );
}

const mapDispatchToProps = dispatch => {
  return {
    createPersonnel: datas => dispatch(createPersonnel(datas))
  };
};

export default connect(
  null,
  mapDispatchToProps
)(ResponsiveDialog);
