import React from "react";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import DashboardIcon from "@material-ui/icons/Dashboard";
import ShoppingCartIcon from "@material-ui/icons/ShoppingCart";
import PeopleIcon from "@material-ui/icons/People";
import BarChartIcon from "@material-ui/icons/BarChart";
import PowerSettingNewIcon from "@material-ui/icons/PowerSettingsNew";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import Divider from "@material-ui/core/Divider";

function sideMenu(props) {
  return (
    <React.Fragment>
      <ListItem button component={Link} to="/">
        <ListItemIcon>
          <DashboardIcon />
        </ListItemIcon>
        <ListItemText primary="Dashboard" />
      </ListItem>
      <ListItem button component={Link} to="/personnel">
        <ListItemIcon>
          <PeopleIcon />
        </ListItemIcon>
        <ListItemText primary="Personnel Manager" />
      </ListItem>
      <ListItem button component={Link} to="/facilities">
        <ListItemIcon>
          <BarChartIcon />
        </ListItemIcon>
        <ListItemText primary="Facilities Manager" />
      </ListItem>
      <ListItem button component={Link} to="/provider">
        <ListItemIcon>
          <ShoppingCartIcon />
        </ListItemIcon>
        <ListItemText primary="Provider Manager" />
      </ListItem>
      <Divider />
      <ListItem
        button
        component={Link}
        to="/signin"
        onClick={() => {
          props.logOut();

          alert("Logged Out!");
        }}
      >
        <ListItemIcon>
          <PowerSettingNewIcon />
        </ListItemIcon>
        <ListItemText primary="Log Out" />
      </ListItem>
    </React.Fragment>
  );
}

const mapDispatchToProps = dispatch => {
  return {
    logOut: () => dispatch({ type: "LOG_OUT" })
  };
};

export default connect(
  null,
  mapDispatchToProps
)(sideMenu);
