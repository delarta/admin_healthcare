import React from "react";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import { useTheme } from "@material-ui/core/styles";
import InfoIcon from "@material-ui/icons/Info";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import { connect } from "react-redux";
function ResponsiveDialog(props) {
  const [open, setOpen] = React.useState(false);
  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down("sm"));

  function handleClickOpen() {
    setOpen(true);
    props.viewData();
  }

  function handleClose() {
    setOpen(false);
  }

  return (
    <div>
      <Button
        size="small"
        color="primary"
        aria-label="Delete"
        onClick={handleClickOpen}
      >
        <InfoIcon />
      </Button>
      <Dialog
        fullScreen={fullScreen}
        open={open}
        onClose={handleClose}
        aria-labelledby="responsive-dialog-title"
      >
        <DialogTitle id="responsive-dialog-title">
          {"View Personnel"}
        </DialogTitle>
        <DialogContent>
          <List component="nav" aria-label="Main mailbox folders">
            {props.personnel !== undefined ? (
              <React.Fragment>
                <ListItem>
                  <ListItemText
                    primary={props.personnel.personnelID}
                    secondary="Personnel ID"
                  />
                </ListItem>
                <ListItem>
                  <ListItemText
                    primary={props.personnel.firstName}
                    secondary="First Name"
                  />
                </ListItem>
                <ListItem>
                  <ListItemText
                    primary={props.personnel.lastName}
                    secondary="Last Name"
                  />
                </ListItem>
                <ListItem>
                  <ListItemText
                    primary={props.personnel.type}
                    secondary="Type"
                  />
                </ListItem>
                <ListItem>
                  <ListItemText
                    primary={props.personnel.NRIC}
                    secondary="NRIC"
                  />
                </ListItem>
                <ListItem>
                  <ListItemText
                    primary={props.personnel.designation}
                    secondary="Designation"
                  />
                </ListItem>
                <ListItem>
                  <ListItemText
                    primary={props.personnel.mobileNumber}
                    secondary="Mobile Number"
                  />
                </ListItem>
                <ListItem>
                  <ListItemText
                    primary={props.personnel.descriptionAndRemarks}
                    secondary="Desription and Remark"
                  />
                </ListItem>
                <ListItem>
                  <ListItemText
                    primary={props.personnel.MCRNumber}
                    secondary="MCR Number"
                  />
                </ListItem>
              </React.Fragment>
            ) : (
              "Loading..."
            )}
          </List>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Close
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}

const mapStateToProps = state => ({
  personnel: state.personnel.viewData
});

export default connect(mapStateToProps)(ResponsiveDialog);
