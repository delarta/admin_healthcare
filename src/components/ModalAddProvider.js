import React from "react";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import { useTheme } from "@material-ui/core/styles";
import {
  TextField,
  MenuItem
} from "@material-ui/core";
import { useCreateProvider } from "../Hooks/CustomHooks";
import { createProvider } from "../redux/actions/provider";
import { connect } from "react-redux";

function ResponsiveDialog(props) {
  const [open, setOpen] = React.useState(false);

  const create = () => {
    const datas = {
      clinicName: inputs.clinicName[0],
      type: inputs.type[0],
      contact: {
        telephone: inputs.telephone[0],
        email: inputs.email[0]
      },
      gstStatus: { hasGST: inputs.gstStatus[0] }
    };
    props.createProvider(datas);
    handleClose();
  };

  const { inputs, handleInputChange, handleSubmit } = useCreateProvider(create);
  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down("sm"));

  function handleClickOpen() {
    setOpen(true);
  }

  function handleClose() {
    setOpen(false);
  }

  return (
    <div>
      <Button color="primary" variant="contained" onClick={handleClickOpen}>
        Create Provider
      </Button>
      <Dialog
        fullScreen={fullScreen}
        open={open}
        onClose={handleClose}
        aria-labelledby="responsive-dialog-title"
      >
        <DialogTitle id="responsive-dialog-title">
          {"Create Provider"}
        </DialogTitle>
        <DialogContent>
          <form onSubmit={handleSubmit}>
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="clinicName"
              label="Clinic Name"
              name="clinicName"
              autoComplete="clinicName"
              autoFocus
              onChange={handleInputChange}
              value={inputs.clinicName}
            />
            <TextField
              id="type"
              select
              label="Type"
              name="type"
              helperText="Please select provider type"
              margin="normal"
              variant="outlined"
              onChange={handleInputChange}
              value={inputs.type}
            >
              <MenuItem value={"GP"}>General Practitioner</MenuItem>
              <MenuItem value={"SP"}>Specialist</MenuItem>
              <MenuItem value={"CM"}>Chinesse Medicine</MenuItem>
              <MenuItem value={"DT"}>Dental</MenuItem>
              <MenuItem value={"LB"}>Lab</MenuItem>
              <MenuItem value={"SM"}>XRay</MenuItem>
            </TextField>
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              type="email"
              id="email"
              label="Email Address"
              name="email"
              autoComplete="email"
              onChange={handleInputChange}
              value={inputs.email}
            />
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="telephone"
              label="Mobile Number"
              name="telephone"
              autoComplete="telephone"
              onChange={handleInputChange}
              value={inputs.telephone}
            />
            <TextField
              id="gstStatus"
              select
              label="Has GST ?"
              name="gstStatus"
              helperText="Please select gstStatus"
              margin="normal"
              variant="outlined"
              onChange={handleInputChange}
              value={inputs.gstStatus}
            >
              <MenuItem value={true}>Yes</MenuItem>
              <MenuItem value={false}>No</MenuItem>
            </TextField>
            {/* <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="gstStatus"
              label="gstStatus"
              name="gstStatus"
              autoComplete="gstStatus"
              onChange={handleInputChange}
              value={inputs.gstStatus}
            /> */}

            <Button type="submit" fullWidth variant="contained" color="primary">
              Create
            </Button>
          </form>
        </DialogContent>
      </Dialog>
    </div>
  );
}

const mapDispatchToProps = dispatch => {
  return {
    createProvider: datas => dispatch(createProvider(datas))
  };
};

export default connect(
  null,
  mapDispatchToProps
)(ResponsiveDialog);
