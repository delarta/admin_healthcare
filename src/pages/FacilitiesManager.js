import React, { useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import CssBaseline from "@material-ui/core/CssBaseline";
import Container from "@material-ui/core/Container";
import Navbar from "../components/Navbar";
import MUIDataTable from "mui-datatables";
import ModalAddFacility from "../components/ModalAddFacility";
import ModalDelete from "../components/ModalDelete";
import { connect } from "react-redux";
import { showFacility, deleteFacility } from "../redux/actions/facility";
import Grid from "@material-ui/core/Grid";
import CircularProgress from "@material-ui/core/CircularProgress";

import ModalUpdateFacility from "../components/ModalUpdateFacility";
const drawerWidth = 240;

const useStyles = makeStyles(theme => ({
  root: {
    display: "flex"
  },
  toolbar: {
    paddingRight: 24 // keep right padding when drawer closed
  },
  toolbarIcon: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end",
    padding: "0 8px",
    ...theme.mixins.toolbar
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen
    })
  },
  menuButton: {
    marginRight: 36
  },
  menuButtonHidden: {
    display: "none"
  },
  title: {
    flexGrow: 1
  },
  appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    height: "100vh",
    overflow: "auto"
  },
  container: {
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4)
  },
  paper: {
    padding: theme.spacing(2),
    display: "flex",
    overflow: "auto",
    flexDirection: "column"
  },
  fixedHeight: {
    height: 240
  },
  fab: {
    margin: theme.spacing(1)
  },
  extendedIcon: {
    marginRight: theme.spacing(1)
  }
}));

function FacilitiesManager(props) {
  props.auth.isAdmin ||
    (localStorage.getItem("Token") === null && props.history.push("/signin"));
  useEffect(() => {
    props.showFacility();
  }, []);
  const columns = [

    {
      name: "ID",
      label: "ID"
    },
    {
      name: "facilityID",
      label: "Facility ID"
    },
    {
      name: "facilityName",
      label: "Facility Name"
    },
    {
      name: "ID",
      label: "Actions",
      options: {
        customBodyRender: value => (
          <Grid container spacing={0}>
            <Grid item xs>
              <ModalUpdateFacility ID={value} className={classes.fab} />
            </Grid>
            <Grid item xs>
              <ModalDelete
                deleteData={() => props.deleteFacility(value)}
                className={classes.fab}
              />
            </Grid>
          </Grid>
        )
      }
    }
  ];

  const facilities = props.facility.data;
  const options = {
    print: false,
    download: false,
    viewColumns: false,
    selectableRows: false,
    responsive: "scroll"
  };

  const classes = useStyles();
  return (
    <div className={classes.root}>
      <CssBaseline />
      <Navbar />

      <main className={classes.content}>
        <div className={classes.appBarSpacer} />
        <Container maxWidth="lg" className={classes.container}>
          {facilities !== undefined ? (
            <React.Fragment>
              <ModalAddFacility />
              <MUIDataTable
                title={"Facilities List"}
                data={facilities}
                columns={columns}
                options={options}
              />
            </React.Fragment>
          ) : (
            <React.Fragment>
              <CircularProgress />
            </React.Fragment>
          )}
        </Container>
      </main>
    </div>
  );
}

const mapStateToProps = state => ({
  facility: state.facility,
  auth: state.auth
});

const mapDispatchToProps = dispatch => {
  return {
    showFacility: () => dispatch(showFacility()),
    deleteFacility: id => dispatch(deleteFacility(id))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FacilitiesManager);
