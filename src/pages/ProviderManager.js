import React, { useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import CssBaseline from "@material-ui/core/CssBaseline";
import Container from "@material-ui/core/Container";
import Navbar from "../components/Navbar";
import MUIDataTable from "mui-datatables";
import ModalAddProvider from "../components/ModalAddProvider";
import ModalViewProvider from "../components/ModalViewProvider";
import ModalDelete from "../components/ModalDelete";
import { connect } from "react-redux";
import { showProvider, deleteProvider, viewProvider } from "../redux/actions/provider";
import CircularProgress from "@material-ui/core/CircularProgress";
import Grid from "@material-ui/core/Grid";

const drawerWidth = 240;

const useStyles = makeStyles(theme => ({
  root: {
    display: "flex"
  },
  toolbar: {
    paddingRight: 24 // keep right padding when drawer closed
  },
  toolbarIcon: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end",
    padding: "0 8px",
    ...theme.mixins.toolbar
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen
    })
  },
  menuButton: {
    marginRight: 36
  },
  menuButtonHidden: {
    display: "none"
  },
  title: {
    flexGrow: 1
  },
  appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    height: "100vh",
    overflow: "auto"
  },
  container: {
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4)
  },
  paper: {
    padding: theme.spacing(2),
    display: "flex",
    overflow: "auto",
    flexDirection: "column"
  },
  fixedHeight: {
    height: 240
  }
}));

function ProviderManager(props) {
  
  useEffect(() => {
    localStorage.getItem("Token") === null &&
      props.history.push("/signin");
    props.showProvider();
  }, []);
  const columns = [
    {
      name: "ID",
      label: "ID"
    },
    {
      name: "clinicName",
      label: "Clinic Name"
    },
    {
      name: "type",
      label: "Type"
    },
    {
      name: "gstStatus.hasGST",
      label: "Has GST",
      options: {
        customBodyRender: value => (value === true ? "True" : "False")
      }
    },
    {
      name: "ID",
      label: "Actions",
      options: {
        customBodyRender: value => (
          <Grid container spacing={3}>
            <Grid item xs>
              <ModalViewProvider
                viewData={() => props.viewProvider(value)}
              />
            </Grid>
            <Grid item xs>
            <ModalDelete deleteData={() => props.deleteProvider(value)} />
            </Grid>
          </Grid>
        )
      }
    }
  ];

  const providers = props.provider.data;
  const options = {
    print: false,
    download: false,
    viewColumns: false,
    selectableRows: false,
    responsive: "scroll"
  };

  const classes = useStyles();
  return (
    <div className={classes.root}>
      <CssBaseline />
      <Navbar />

      <main className={classes.content}>
        <div className={classes.appBarSpacer} />

        <Container maxWidth="lg" className={classes.container}>
          {providers !== undefined ? (
            <React.Fragment>
              <ModalAddProvider />
              <MUIDataTable
                title={"Provider List"}
                data={providers}
                columns={columns}
                options={options}
              />
            </React.Fragment>
          ) : (
            <CircularProgress />
)}
        </Container>
      </main>
    </div>
  );
}

const mapStateToProps = state => ({
  provider: state.provider
});

const mapDispatchToProps = dispatch => {
  return {
    showProvider: () => dispatch(showProvider()),
    deleteProvider: id => dispatch(deleteProvider(id)),
    viewProvider: id => dispatch(viewProvider(id))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProviderManager);
