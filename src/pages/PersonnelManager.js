import React, { useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import CssBaseline from "@material-ui/core/CssBaseline";
import Container from "@material-ui/core/Container";
import Navbar from "../components/Navbar";
import MUIDataTable from "mui-datatables";
import ModalAdd from "../components/ModalAdd";
import ModalViewPersonnel from "../components/ModalViewPersonnel";
import ModalDelete from "../components/ModalDelete";
import { connect } from "react-redux";
import {
  showPersonnel,
  deletePersonnel,
  viewPersonnel
} from "../redux/actions/personnel.js";
import CircularProgress from "@material-ui/core/CircularProgress";
import Grid from "@material-ui/core/Grid";

const drawerWidth = 240;

const useStyles = makeStyles(theme => ({
  root: {
    display: "flex"
  },
  toolbar: {
    paddingRight: 24 // keep right padding when drawer closed
  },
  toolbarIcon: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end",
    padding: "0 8px",
    ...theme.mixins.toolbar
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen
    })
  },
  menuButton: {
    marginRight: 36
  },
  menuButtonHidden: {
    display: "none"
  },
  title: {
    flexGrow: 1
  },
  appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    height: "100vh",
    overflow: "auto"
  },
  container: {
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4)
  },
  paper: {
    padding: theme.spacing(2),
    display: "flex",
    overflow: "auto",
    flexDirection: "column"
  },
  fixedHeight: {
    height: 240
  },
  fab: {
    margin: theme.spacing(1)
  }
}));

function Personnel(props) {
  useEffect(() => {
    localStorage.getItem("Token") === null &&
      this.props.history.push("/signin");
    props.showPersonnel();
  }, []);
  const columns = [
    {
      name: "ID",
      label: "ID"
    },
    {
      name: "firstName",
      label: "Name"
    },
    {
      name: "NRIC",
      label: "NRIC"
    },
    {
      name: "MCRNumber",
      label: "MCR Number"
    },
    {
      name: "type",
      label: "Type"
    },
    {
      name: "designation",
      label: "Designation"
    },
    {
      name: "mobileNumber",
      label: "Mobile Number"
    },
    {
      name: "ID",
      label: "Actions",
      options: {
        customBodyRender: value => (
          <Grid container spacing={3}>
            <Grid item xs>
              <ModalViewPersonnel
                viewData={() => props.viewPersonnel(value)}
              />
            </Grid>
            <Grid item xs>
              <ModalDelete
                deleteData={() => props.deletePersonnel(value)}
                className={classes.fab}
              />
            </Grid>
          </Grid>
        )
      }
    }
  ];

  const personnels = props.personnel.data;
  const options = {
    print: false,
    download: false,
    viewColumns: false,
    selectableRows: false,
    responsive: "scroll"
  };

  const classes = useStyles();
  return (
    <div className={classes.root}>
      <CssBaseline />
      <Navbar />

      <main className={classes.content}>
        <div className={classes.appBarSpacer} />
        <Container maxWidth="lg" className={classes.container}>
          {personnels !== undefined ? (
            <React.Fragment>
              <ModalAdd />
              <MUIDataTable
                title={"Personnel List"}
                data={personnels}
                columns={columns}
                options={options}
              />
            </React.Fragment>
          ) : (
            <CircularProgress color="primary" />
          )}
        </Container>
      </main>
    </div>
  );
}

const mapStateToProps = state => ({
  personnel: state.personnel
});

const mapDispatchToProps = dispatch => {
  return {
    showPersonnel: () => dispatch(showPersonnel()),
    deletePersonnel: id => dispatch(deletePersonnel(id)),
    viewPersonnel: id => dispatch(viewPersonnel(id))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Personnel);
