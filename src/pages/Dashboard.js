import React, { useEffect } from "react";
import clsx from "clsx";
import { makeStyles } from "@material-ui/core/styles";
import CssBaseline from "@material-ui/core/CssBaseline";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import Navbar from "../components/Navbar";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { getPersonnel, getFacility, getProvider } from "../redux/actions/admin";

const drawerWidth = 240;

const useStyles = makeStyles(theme => ({
  root: {
    display: "flex"
  },
  toolbar: {
    paddingRight: 24 // keep right padding when drawer closed
  },
  toolbarIcon: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end",
    padding: "0 8px",
    ...theme.mixins.toolbar
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen
    })
  },
  menuButton: {
    marginRight: 36
  },
  menuButtonHidden: {
    display: "none"
  },
  title: {
    flexGrow: 1
  },
  appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    height: "100vh",
    overflow: "auto"
  },
  container: {
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4)
  },
  paper: {
    padding: theme.spacing(2),
    display: "flex",
    overflow: "auto",
    flexDirection: "column"
  },
  fixedHeight: {
    height: 240
  }
}));

function Dashboard(props) {
  useEffect(() => {
    localStorage.getItem("Token") === null && props.history.push("/signin");
    props.getPersonnel();
    props.getFacility();
    props.getProvider();
  }, []);

  if (!localStorage.getItem("firstLoad")) {
    localStorage["firstLoad"] = true;
    window.location.reload();
  }
  const classes = useStyles();
  const personnels = props.personnel;
  const providers = props.provider;
  const facilities = props.facility;
  const doctors = props.personnel.filter(
    item => item.type.toLowerCase() === "doctor"
  );

  const fixedHeightPaper = clsx(classes.paper, classes.fixedHeight);

  return (
    <div className={classes.root}>
      <CssBaseline />
      <Navbar />

      <main className={classes.content}>
        <div className={classes.appBarSpacer} />

        <Container maxWidth="lg" className={classes.container}>
          <Grid container spacing={3}>
            {/* Personnel Numbers */}
            <Grid item xs={12} md={6} lg={3}>
              <Paper className={fixedHeightPaper}>
                <Typography component="h4" variant="h6" color="inherit">
                  {" "}
                  Personnels{" "}
                </Typography>
                <Typography component="h4" variant="h1" color="primary">
                  {personnels !== undefined ? personnels.length : "Loading..."}
                </Typography>
              </Paper>
            </Grid>
            {/* Provider Numbers */}
            <Grid item xs={12} md={6} lg={3}>
              <Paper className={fixedHeightPaper}>
                <Typography component="h4" variant="h6" color="inherit">
                  {" "}
                  Providers{" "}
                </Typography>
                <Typography component="h4" variant="h1" color="primary">
                  {providers !== undefined ? providers.length : null}
                </Typography>
              </Paper>
            </Grid>
            {/* Facilities Numbers */}
            <Grid item xs={12} md={6} lg={3}>
              <Paper className={fixedHeightPaper}>
                <Typography component="h4" variant="h6" color="inherit">
                  {" "}
                  Facilities{" "}
                </Typography>
                <Typography component="h4" variant="h1" color="primary">
                  {facilities !== undefined ? facilities.length : null}
                </Typography>
              </Paper>
            </Grid>
            {/* Doctors Numbers */}
            <Grid item xs={12} md={6} lg={3}>
              <Paper className={fixedHeightPaper}>
                <Typography component="h4" variant="h6" color="inherit">
                  {" "}
                  Doctors{" "}
                </Typography>
                <Typography component="h4" variant="h1" color="primary">
                  {doctors !== undefined ? doctors.length : null}
                </Typography>
              </Paper>
            </Grid>
          </Grid>
        </Container>
        <Typography variant="body2" color="textSecondary" align="center">
          Delarta &copy; 2019
        </Typography>
      </main>
    </div>
  );
}

const mapStateToProps = state => ({
  personnel: state.admin.personnels,
  provider: state.admin.providers,
  facility: state.admin.facilities
});

const mapDispatchToProps = dispatch => {
  return {
    getPersonnel: () => dispatch(getPersonnel()),
    getFacility: () => dispatch(getFacility()),
    getProvider: () => dispatch(getProvider())
  };
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Dashboard)
);
