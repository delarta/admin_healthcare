# **Admin Healtcare Project**

This project is a web application to facilitate the online management of doctors, clinic and network. This project built using React JS

## **Quick start**

Run this command in terminal

```
$ git clone https://delarta@bitbucket.org/delarta/admin_healthcare.git
$ cd admin_healthcare
$ npm install or yarn install
$ npm start or yarn start
```

## **How to use**

This section is a walkthrough to the feature: 
1. Login
2. Show Dashboard Statistic
3. Show Personnel List
4. Create Personnel
5. View Personnel Detail
6. Delete Personnel
7. Show Facility List
8. Create Facility
9. Update Facility
10. Delete Facility
11. Show Provider List
12. Create Provider
13. View Provider Detail
14. Delete Personnel

Here's the step's description

### **Login**

![Login Page](https://res.cloudinary.com/dtpnqygai/image/upload/v1563767781/MHC%20Test/1._Sign_In_asnpqc.png)

In the login page we will need to input the username and password that has been created in the database. The inputs then authenticated in the database.

### **Dashboard**

![Dashboard Page](https://res.cloudinary.com/dtpnqygai/image/upload/v1563767726/MHC%20Test/2._Dashboard_rbksfg.png)

The dashboard will show the total amount of Personnels, Providers, Facilities, and Doctors in the database.

### **Personnel**

In the personnel page there's 4 feature : ***show, create, view, and delete***  

**1. Show Personnel**

![Show Personnel](https://res.cloudinary.com/dtpnqygai/image/upload/v1563767758/MHC%20Test/3._Show_Personnel_uttrpw.png)

Personnel list is shown using table. 

**2. Create Personnel**

![Create Personnel](https://res.cloudinary.com/dtpnqygai/image/upload/v1563767712/MHC%20Test/4._Create_Personnel_ajlelf.png)

Create personnel is a modal view. The inputs are *First Name, Last Name, NRIC, Type, Designation, Mobile Number, Description and Remark, Years of Experience, Certification Name, Remarks, default Speciality, and Sub Speciality*

**3. View Personnel**

![View Personnel](https://res.cloudinary.com/dtpnqygai/image/upload/v1563767742/MHC%20Test/6._View_Personnel_idxrpw.png)

View Personnel is a modal containing the detail of the personnel. Modal is shown when user click the blue info action button on the right.

**4. Delete Personnel**

![Delete Personnel](https://res.cloudinary.com/dtpnqygai/image/upload/v1563767743/MHC%20Test/7._Delete_Personnel_akeoxd.png)

Delete personnel is used to delete the personnel data. It will prompt the user to double check the action.

### **Facility**

In the facility page there are 4 feature : 
***show, create, update, and delete***  

**1. Show Facility**

![Show Facility](https://res.cloudinary.com/dtpnqygai/image/upload/v1563767767/MHC%20Test/8._Show_Facilities_dhcnyx.png)

Facility list is shown using table. There are *ID, Facility ID and Facility Name* data.

**2. Create Facility**

![Create Facility](https://res.cloudinary.com/dtpnqygai/image/upload/v1563767814/MHC%20Test/9._Create_Facility_cns7cn.png)

Create Facility is a modal view. The input is *Facility Name*.

**3. Update Facility**

![Update Facility](https://res.cloudinary.com/dtpnqygai/image/upload/v1563767769/MHC%20Test/11._Update_Facility_ikt7f1.png)

Update Facility is a modal containing form input to update the data selected.

**4. Delete Facility**

![Delete Facility](https://res.cloudinary.com/dtpnqygai/image/upload/v1563767822/MHC%20Test/12._Delete_Facility_m7tcxw.png)

Delete facility is used to delete the facility data. It will prompt the user to double check the action.

### **Provider**

**1. Show Provider**

![Show Provider](https://res.cloudinary.com/dtpnqygai/image/upload/v1563767804/MHC%20Test/13._Show_Provider_odqykb.png)

Facility list from the api is shown using table. There are *ID, Class Name, Type, and has GST* data.

**2. Create Provider**

![Create Provider](https://res.cloudinary.com/dtpnqygai/image/upload/v1563767793/MHC%20Test/14._Create_Provider_qqr14d.png)

Create Facility is a modal view. The input required are *Class Name, Type, Email, Mobile Number and Has GST*. The *email, mobile number and has GST* input is required but it won't update in the database.

**3. View Provider**

![Update Provider](https://res.cloudinary.com/dtpnqygai/image/upload/v1563767819/MHC%20Test/16._View_Provider_td1bcr.png)

View Personnel is a modal containing detailed information of the provider selected.

**4. Delete Provider**

![Delete Provider](https://res.cloudinary.com/dtpnqygai/image/upload/v1563767819/MHC%20Test/17._Delete_Provider_yhvfwq.png)

Delete provider is used to delete the facility data. It will prompt the user to double check the action.

## **Tools**

This application is using tools to help the development process.

**Styling Component**
* [Material UI](https://material-ui.com/) - The styiling used in the application, it's based on Material UI by Google
* [MUI Datatables](https://github.com/gregnb/mui-datatables) - It give table the ability to sort, search and responsive

**Packages**
* [redux](https://redux.js.org/) - State management tools for modern framework
* [redux-thunk](https://github.com/reduxjs/redux-thunk) - Thunk middleware for Redux
* [axios](https://github.com/axios/axios) - Promise based HTTP client for the browser and node.js

```
Copyright 2019 Delarta Tok Adin
```